package com.example.s524967.simpleplanview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

/**
 * Created by S525050 on 9/8/2016.
 */
public class Categories extends AppCompatActivity{
    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private CategoriesListAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories);
        mRecyclerView = (RecyclerView) findViewById(R.id.categories_list);
        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
        mAdapter = new CategoriesListAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new CategoriesListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                if (position == 2) {
                    Intent it = new Intent(Categories.this, TypeofProblemSimplePlanView.class);
                    startActivity(it);
                }
            }
        });
    }
}
