package com.example.s524967.simpleplanview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        Button btn_catgories = (Button) findViewById(R.id.button_categories);

        btn_catgories.setOnClickListener(new Button.OnClickListener()
        {

            @Override
            public void onClick(View view) {
                Intent categories_page = new Intent(HomeActivity.this,Categories.class);
                startActivity(categories_page);
            }
        });
    }
}
