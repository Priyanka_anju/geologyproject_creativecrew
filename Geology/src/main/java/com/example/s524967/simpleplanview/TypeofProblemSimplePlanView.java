package com.example.s524967.simpleplanview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TypeofProblemSimplePlanView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_typeof_problem_simple_plan_view);
    }

    public void quadrantOnClick(View view) {
        //Submit button on click
        Button submitButton = (Button) view;
        Intent submitButtonIntent = new Intent(TypeofProblemSimplePlanView.this, SimplePlanView.class);
        startActivity(submitButtonIntent);

    }
}
