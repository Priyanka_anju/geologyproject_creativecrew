 
Section - #5
Project name: Geology Mobile Application
Software Requirements Specification
The purpose of the software requirement specification is to reduce the communication gap between the client and the developers. Software Requirement Specification is the medium through which the client and user needs are accurately specified. It forms the basis of software development.
Draft - Final
Creative Crew Members:


1.	Prashanth Reddy Malgireddy
2.	Priyanka Anjuri
3.	Rajesh Karupakala
4.	Narender Reddy Pendri
5.	Sphoorthi Gaddam
6.	Ritwik Akula
7.	Srivardhan Rao Thakkallapally
8.	Mohan Sudheer Kilaru
9.	Sunil Reddy Vatti
10.	Venkata Sravya Kancharla

 
Signatories
Team members: 

1.	Prashanth Reddy Malgireddy	 
2.	Priyanka Anjuri	 
3.	Rajesh Karupakala	 
4.	Narender Reddy Pendri	 
5.	Sphoorthi Gaddam	 
6.	Ritwik Akula	 
7.	Srivardhan Rao Thakkallapally	 
8.	Mohan Sudheer Kilaru	 
9.	Sunil Reddy Vatti	 
10.	Venkata Sravya Kancharla	 

Client Signature:
Dr. Arghya Goswami 	

---------------------------------------		


Table of Contents
Signatories	2
1.Introduction	4
1.1 Purpose	4
1.2 Keywords in the document	4
1.3 Project scope	5
2. System Description:	6
2.1 Problem Description:	6
2.2 Proposed Solution:	6
2.3 Assumptions and Dependencies:	6
3. Specific Requirements	7
3.1 Functional Requirements	7
3.2 Optional Requirements	7
3.3 Non-Functional Requirements	7


























1.	Introduction
1.1. Purpose
As the geology students deals with lot of variety of problems. There are various tools available for them to solve the problems, but there is not even a single tool available that can solve multiple problems related to geology. Each tool is designed to solve a specific type of problem. For example Edu Mine is a tool that can solve three point problem. This tool just gives the values of dip and strike notation but does not give the representation of those three points. This geology mobile application will be a platform to the geology students where one can solve three point problem, trigonometric problem, and simple plan view problem. Not only solving problem each problem will be represented graphically with appropriate dip and strike notation.

       1.2. Keywords in the document
Bearing	:	A horizontal angle measured east or west of true north or south.
Azimuth	:	A horizontal angle measured clockwise from true north.
Attitude	:	The orientation of a plane or line in space, commonly specified by its bearing or azimuth.
Trend	:	The direction of a line in a horizontal plane, commonly specified by its bearing.
Strike	:	The trend of a horizontal line on an inclined plane.
Inclination	:	The vertical angle, measured downward from the horizontal, to a plane or line.
Dip	:	The inclination of the line of greatest slope on an inclined plane. It is always perpendicular the strike.

Apparent Dip	:	The inclination of a line on an inclined plane measured in a direction oblique (0.1-89.9°) to the strike direction. Apparent dip is always less than true dip.



           1.3. Project scope

Product Characteristics and requirements:
•	The mobile applications will allow the users to view list of problem types.
•	Users will be able to select the required problem type from the given problem types.
•	The input for each problem can be entered by the users using the given interface.
•	The users will be given a diagram with labels for the problems which require a clarity on input parameters.
•	The application will provide output based on the input entered by the users.
•	The application will also be able to provide a map view, scale, plan and cross-sectional views for the block diagrams.
•	The users can also zoom the scale, use gestures, and switch between plan views, cross-sectional view and can rotate the views.
•	The mobile application will also encourage users to save their output in the form of pdf/jpeg.
•	The application will also give a list of previously worked problems and the users can open them to work on them.
Project Deliverables:
•	The main deliverables are the Software Requirements Specification, Charters and signatures of the client and the team members, schedule, WBS, other required documents.
•	The application will provide problem types, their required input types, and the output for the problem types and their inputs.
•	The software needed are android, word, spread documents, tools and software required for any work related tasks. 

 

2.	System Description
2.1.	 Problem Description

Finding solutions for different problems in geology is important for many geology students and staff. As the usage of pre-defined tools and applications arose this project is developed for android users.
There is a requirement to find solutions for different kinds of problems like

Mandatory:
1. Strike/Dip Notation (with definitions)
2. Trigonometric Problems
	4A. Simple
		4A1 - 5 parameters
		4A2 - 8 Parameters
3. Simple Plan View Problems
	3A. with Structural Symbols
	3B. Without Structural Symbols and also drawing like room plan.
4. Trigonometric Problems
	4A. Simple 
	4B. Acute/Obtuse Angle laws of sine and cosine....
5. Three point / Borehole Problems 

Optional:
6. Block diagram
7. True Dip/Apparent Dip Problems

2.2.	 Proposed Solution
The proposed system contains different problems and based on the user need the inputs are asked for the user. After giving the input the solutions is generated for that particular problem.
2.3.	 Assumptions and Dependencies
This project assumes that the user has prior experience about geology oriented problems.
The project assumes that if the user needs to re-open the already solved problem, the user should save the problem before that.
 

3.	Specific Requirements
3.1.	 Functional Requirements
i.	The System should be able to display options to select the type of problem.

Mandatory:
1. Strike/Dip Notation (with definitions)
2. Trigonometric Problems
	4A. Simple
		4A1 - 5 parameters
		4A2 - 8 Parameters
3. Simple Plan View Problems
	3A. with Structural Symbols
	3B. Without Structural Symbols and also drawing like room plan.
4. Trigonometric Problems
	4A. Simple 
	4B. Acute/Obtuse Angle laws of sine and cosine....
5. Three point / Borehole Problems 

Optional:
6. Block diagram
7. True Dip/Apparent Dip Problems

ii.	The System should be able to display diagram with labels
iii.	The System should be able take input from the user
iv.	The System should be able to view the map view.
v.	The system should be able to view the scale.
vi.	The System should be able to zoom the scale 
vii.	The System should allow user to rotate the views.
viii.	The System should be able to display the plan view.
ix.	The System should be able to display the cross sectional view.
x.	The System able to show static symbols for different kinds of problems.
xi.	The System able to save and reopen the problem document.
xii.	The System able to save the problem solution into pdf.
3.2.	 Optional Requirements
The following problem types are optional:
i.	Block
ii.	True Dip/Apparent
3.3.	 Non-Functional Requirements
i.	Performance: The processing of inputs and finding solution should be fast.
ii.	Cost: The Cost of the project development should be minimum.
iii.	Minimum Software Requirement:
a.	Operating Systems: Android
b.	Programing languages: JDK
iv.	Minimum Hardware Requirement:
a.	Processor: i3
b.	RAM: 2GB
c.	Hard Disk: 40 GB






